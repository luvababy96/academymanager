package com.lsw.academymanager.entity;

import com.lsw.academymanager.enums.MachineCategory;
import com.lsw.academymanager.enums.MachineCondition;
import com.lsw.academymanager.enums.MachinePlace;
import com.lsw.academymanager.interfaces.CommonModelBuilder;
import com.lsw.academymanager.model.MachineRequest;
import com.lsw.academymanager.model.MachineUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Machine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 30)
    private MachineCategory category;

    @Column(nullable = false, length = 15)
    private String number;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MachinePlace place;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    private String size;

    @Column(nullable = false)
    private Integer price;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MachineCondition condition;

    @Column(nullable = false)
    private LocalDate buyDate;

    private LocalDate disuseDate;

    public void putMachine(MachineUpdateRequest request) {
        this.name = request.getName();
    }

    private Machine(MachineBuilder builder){
        this.category = builder.category;
        this.number = builder.number;
        this.place = builder.place;
        this.name = builder.name;
        this.size = builder.size;
        this.price = builder.price;
        this.condition = builder.condition;
        this.buyDate = builder.buyDate;
    }

    public static class MachineBuilder implements CommonModelBuilder<Machine> {
        private final MachineCategory category;
        private final String number;
        private final MachinePlace place;
        private final String name;
        private final String size;
        private final Integer price;
        private final MachineCondition condition;
        private final LocalDate buyDate;

        public MachineBuilder(MachineRequest machineRequest) {
            this.category = machineRequest.getCategory();
            this.number = machineRequest.getNumber();
            this.place = machineRequest.getPlace();
            this.name = machineRequest.getName();
            this.size = machineRequest.getSize();
            this.price = machineRequest.getPrice();
            this.condition = MachineCondition.GOOD;
            this.buyDate = machineRequest.getBuyDate();
        }

        @Override
        public Machine build() {
            return new Machine(this);
        }
    }







}
