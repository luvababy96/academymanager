package com.lsw.academymanager.entity;

import com.lsw.academymanager.interfaces.CommonModelBuilder;
import com.lsw.academymanager.model.HistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "machineId", nullable = false)
    private Machine machine;


    @Column(nullable = false)
    private LocalDate useDate;

    @Column(nullable = false,length = 2)
    private Integer useTime;

    @Column(nullable = false,length = 15)
    private String useMemberName;

    private UsageHistory(UsageHistoryBuilder builder) {
        this.machine = builder.machine;
        this.useDate = builder.useDate;
        this.useTime = builder.useTime;
        this.useMemberName = builder.useMemberName;
    }

    public static class UsageHistoryBuilder implements CommonModelBuilder<UsageHistory> {
        private final Machine machine;
        private final LocalDate useDate;
        private final Integer useTime;
        private final String useMemberName;

        public UsageHistoryBuilder(Machine machine,HistoryRequest historyRequest){
           this.machine = machine;
           this.useDate = historyRequest.getUseDate();
           this.useTime = historyRequest.getUseTime();
           this.useMemberName = historyRequest.getUseMemberName();
        }

        @Override
        public UsageHistory build() {
            return new UsageHistory(this);
        }
    }
}
