package com.lsw.academymanager.repository;

import com.lsw.academymanager.entity.Machine;
import com.lsw.academymanager.enums.MachineCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByCategoryOrderByIdDesc(MachineCategory category);

}
