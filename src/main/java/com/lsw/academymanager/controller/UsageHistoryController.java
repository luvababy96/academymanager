package com.lsw.academymanager.controller;

import com.lsw.academymanager.entity.Machine;
import com.lsw.academymanager.model.HistoryRequest;
import com.lsw.academymanager.model.ListResult;
import com.lsw.academymanager.model.UsageHistoryItem;
import com.lsw.academymanager.service.MachineService;
import com.lsw.academymanager.service.ResponseService;
import com.lsw.academymanager.service.UsageHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class UsageHistoryController {

    private final MachineService machineService;
    private final UsageHistoryService usageHistoryService;

    @PostMapping("/new/machine-id/{machineId}")
    public String setHistory(@PathVariable long machineId, @RequestBody @Valid HistoryRequest request) {
        Machine machine = machineService.getData(machineId);
        usageHistoryService.setHistory(machine, request);

        return "OK";
    }

    @GetMapping("/all")
    public ListResult<UsageHistoryItem> getUsageHistories() {
        return ResponseService.getListResult(usageHistoryService.getUsageHistories(),true);
    }

}
