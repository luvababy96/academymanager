package com.lsw.academymanager.controller;

import com.lsw.academymanager.enums.MachineCategory;
import com.lsw.academymanager.model.*;
import com.lsw.academymanager.service.MachineService;
import com.lsw.academymanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {

    private final MachineService machineService;

    @PostMapping("/data")
    public CommonResult setMachine(@RequestBody @Valid MachineRequest request) {
        machineService.setMachine(request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/list/all")
    public ListResult<MachineItem> getMachines() {
        return ResponseService.getListResult(machineService.getMachines(),true);
    }

    @GetMapping("/list/category")
    public ListResult<MachineItem> getMachinesCategory(@RequestParam ("category") MachineCategory category) {
        return ResponseService.getListResult(machineService.getMachinesCategory(category),true);
    }

    @GetMapping("/detail/{id}")
    public SingleResult<MachineResponse> getMachine(@PathVariable long id) {
        return ResponseService.getSingleResult(machineService.getMachine(id));
    }

    @PutMapping("/up-date/{id}")
    public CommonResult putMachine(@PathVariable long id, @RequestBody @Valid MachineUpdateRequest request) {
        machineService.putMachine(id,request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delMachine(@PathVariable long id) {
        machineService.delMachine(id);

        return ResponseService.getSuccessResult();
    }

}
