package com.lsw.academymanager.service;

import com.lsw.academymanager.entity.Machine;
import com.lsw.academymanager.enums.MachineCategory;
import com.lsw.academymanager.model.*;
import com.lsw.academymanager.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.swing.plaf.PanelUI;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {

    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request) {
        Machine addData = new Machine.MachineBuilder(request).build();

        machineRepository.save(addData);
    }

    public Machine getData(long id) {
        return machineRepository.findById(id).orElseThrow();
    }

    public ListResult<MachineItem> getMachines() {
        List<Machine> originList = machineRepository.findAll();

        List<MachineItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new MachineItem.MachineItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<MachineItem> getMachinesCategory(MachineCategory category) {
        List<Machine> originList = machineRepository.findAllByCategoryOrderByIdDesc(category);

        List<MachineItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new MachineItem.MachineItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public MachineResponse getMachine(long id) {
        Machine originData = machineRepository.findById(id).orElseThrow();

        return new MachineResponse.MachineResponseBuilder(originData).build();
    }

    public void putMachine(long id, MachineUpdateRequest request) {
        Machine originData = machineRepository.findById(id).orElseThrow();
        originData.putMachine(request);

        machineRepository.save(originData);
    }

    public void delMachine(long id) {
        machineRepository.deleteById(id);
    }

}
