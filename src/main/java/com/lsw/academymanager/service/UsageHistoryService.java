package com.lsw.academymanager.service;

import com.lsw.academymanager.entity.Machine;
import com.lsw.academymanager.entity.UsageHistory;
import com.lsw.academymanager.model.HistoryRequest;
import com.lsw.academymanager.model.ListResult;
import com.lsw.academymanager.model.UsageHistoryItem;
import com.lsw.academymanager.repository.UsageHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageHistoryService {

    private final UsageHistoryRepository usageHistoryRepository;

    public void setHistory(Machine machine,HistoryRequest request) {
        UsageHistory addData = new UsageHistory.UsageHistoryBuilder(machine,request).build();

        usageHistoryRepository.save(addData);
    }

    public ListResult<UsageHistoryItem> getUsageHistories(){
        List<UsageHistory> originList = usageHistoryRepository.findAll();

        List<UsageHistoryItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new UsageHistoryItem.UsageHistoryItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

}
