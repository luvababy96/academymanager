package com.lsw.academymanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
