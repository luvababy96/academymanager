package com.lsw.academymanager.model;

import com.lsw.academymanager.entity.Machine;
import com.lsw.academymanager.entity.UsageHistory;
import com.lsw.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageHistoryItem {

    private Long id;
    private Long machine;
    private LocalDate useDate;
    private Integer useTime;
    private String useMemberName;

    private UsageHistoryItem(UsageHistoryItemBuilder builder) {
        this.id = builder.id;
        this.machine = builder.machine;
        this.useDate = builder.useDate;
        this.useTime = builder.useTime;
        this.useMemberName = builder.useMemberName;
    }

    public static class UsageHistoryItemBuilder implements CommonModelBuilder<UsageHistoryItem> {
        private final Long id;
        private final Long machine;
        private final LocalDate useDate;
        private final Integer useTime;
        private final String useMemberName;

        public UsageHistoryItemBuilder(UsageHistory usageHistory) {
            this.id = usageHistory.getId();
            this.machine = usageHistory.getMachine().getId();
            this.useDate = usageHistory.getUseDate();
            this.useTime = usageHistory.getUseTime();
            this.useMemberName = usageHistory.getUseMemberName();
        }

        @Override
        public UsageHistoryItem build() {
            return new UsageHistoryItem(this);
        }
    }

}
