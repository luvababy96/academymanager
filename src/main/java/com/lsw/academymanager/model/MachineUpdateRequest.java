package com.lsw.academymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MachineUpdateRequest {

    @NotNull
    @Length(min = 1,max = 20)
    private String name;

}
