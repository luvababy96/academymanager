package com.lsw.academymanager.model;

import com.lsw.academymanager.entity.Machine;
import com.lsw.academymanager.enums.MachineCategory;
import com.lsw.academymanager.enums.MachineCondition;
import com.lsw.academymanager.enums.MachinePlace;
import com.lsw.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineItem {

    private Long id;
    private String category;
    private String number;
    private String place;
    private String name;
    private String condition;


    private MachineItem(MachineItemBuilder builder){
        this.id = builder.id;
        this.category = builder.category;
        this.number = builder.number;
        this.place = builder.place;
        this.name = builder.name;
        this.condition = builder.condition;


    }

    public static class MachineItemBuilder implements CommonModelBuilder<MachineItem> {
        private final Long id;
        private final String category;
        private final String number;
        private final String place;
        private final String name;
        private final String condition;


        public MachineItemBuilder(Machine machine) {
            this.id = machine.getId();
            this.category = machine.getCategory().getCategoryName();
            this.number = machine.getNumber();
            this.place = machine.getPlace().getPlaceName();
            this.name = machine.getName();
            this.condition = machine.getCondition().getConditionName();

        }
        @Override
        public MachineItem build() {
            return new MachineItem(this) ;
        }
    }


}
