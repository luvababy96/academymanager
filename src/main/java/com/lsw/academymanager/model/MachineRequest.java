package com.lsw.academymanager.model;

import com.lsw.academymanager.enums.MachineCategory;
import com.lsw.academymanager.enums.MachineCondition;
import com.lsw.academymanager.enums.MachinePlace;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MachineRequest {

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MachineCategory category;

    @NotNull
    @Length(min = 1,max = 15)
    private String number;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MachinePlace place;

    @NotNull
    @Length(min = 1,max = 20)
    private String name;

    @NotNull
    @Length(min = 1,max = 20)
    private String size;

    @NotNull
    private Integer price;

    @NotNull
    private LocalDate buyDate;
}
