package com.lsw.academymanager.model;

import com.lsw.academymanager.entity.Machine;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class HistoryRequest {

    @NotNull
    private LocalDate useDate;

    @NotNull
    @Min(value = 1)
    @Max(value = 24)
    private Integer useTime;

    @NotNull
    private String useMemberName;



}
