package com.lsw.academymanager.model;

import com.lsw.academymanager.entity.Machine;
import com.lsw.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineResponse {

    private Long id;
    private String number;
    private String place;
    private String name;
    private String size;
    private Integer price;
    private String condition;
    private LocalDate buyDate;
    private LocalDate disuseDate;

    private MachineResponse(MachineResponseBuilder builder) {
        this.id = builder.id;
        this.number = builder.number;
        this.place = builder.place;
        this.name = builder.name;
        this.size = builder.size;
        this.price = builder.price;
        this.condition = builder.condition;
        this.buyDate = builder.buyDate;
        this.disuseDate = builder.disuseDate;
    }

    public static class MachineResponseBuilder implements CommonModelBuilder<MachineResponse> {
        private final Long id;
        private final String number;
        private final String place;
        private final String name;
        private final String size;
        private final Integer price;
        private final String condition;
        private final LocalDate buyDate;
        private final LocalDate disuseDate;

        public MachineResponseBuilder(Machine machine){
            this.id = machine.getId();
            this.number = machine.getNumber();
            this.place = machine.getPlace().getPlaceName();
            this.name = machine.getName();
            this.size = machine.getSize();
            this.price = machine.getPrice();
            this.condition = machine.getCondition().getConditionName();
            this.buyDate = machine.getBuyDate();
            this.disuseDate = machine.getDisuseDate();
        }

        @Override
        public MachineResponse build() {
            return new MachineResponse(this);
        }
    }

}
