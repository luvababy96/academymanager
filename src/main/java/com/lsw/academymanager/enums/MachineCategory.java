package com.lsw.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MachineCategory {

    ELECTRONIC_EQUIPMENT("전자기기"),
    OFFICE_SUPPLIES("사무용품"),
    ETC("기타용품");

    private final String categoryName;
}
