package com.lsw.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MachineCondition {
    GOOD("양호"),
    TROUBLE("고장"),
    DISUSE("폐기");

    private final String conditionName;
}
