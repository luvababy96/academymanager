package com.lsw.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MachinePlace {

    IN_FO("안내데스트"),
    OFFICE("사무실"),
    CLASS_1("교실 1"),
    CLASS_2("교실 2"),
    CLASS_3("교실 3");

    private final String placeName;
}
